var showMessage = function (id, message) {
  document.getElementById(id).innerHTML = `<strong>${message}</strong>`;
};

var kiemTraTrung = function (tenSP, dssv) {
  var index = dssv.findIndex(function (item) {
    return tenSP == item.tenSP;
  });
  if (index == -1) {
    showMessage("spanMaSV", " ");
    // hợp lệ yes
    return true;
  } else {
    showMessage("spanMaSV", "Mã sinh viên đã tồn tại");
    return false;
  }
};

var kiemTraRong = function (idErr, value) {
  if (value.length == 0) {
    showMessage(idErr, "Trường này không được trống");
    return false;
  } else {
    showMessage(idErr, "");
    return true;
  }
};
