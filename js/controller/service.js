var svService = {
  getList: function () {
    return axios({
      url: "https://644fc473ba9f39c6ab6bd8fc.mockapi.io/products",
      method: "GET",
    });
  },
  remove: function (id) {
    return axios({
      url: `https://644fc473ba9f39c6ab6bd8fc.mockapi.io/products/${id}`,
      method: "DELETE",
    });
  },
  create: function (data) {
    return axios({
      url: BASE_URL,
      method: "POST",
      data: data,
    });
  },
};
